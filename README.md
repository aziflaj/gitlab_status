# GitlabStatus

A small Ruby gem that exposes a CLI to check the status of [https://gitlab.com](https://gitlab.com) or [https://about.gitlab.com](https://about.gitlab.com) and reports an average response time after probing the site every 10 seconds for a one minute.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'gitlab_status'
```

And then execute:

```bash
$ bundle install
```

Or install it yourself as:
```bash
$ gem install gitlab_status
```

## Usage

```bash
Commands:
  gitlab-status help [COMMAND]  # Describe available commands or one specific command
  gitlab-status scan            # Checks the status of Gitlab
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/aziflaj/gitlab_status. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [Contributor Covenant](http://contributor-covenant.org) code of conduct.

## License

The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the GitlabStatus project’s codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/aziflaj/gitlab_status/blob/master/CODE_OF_CONDUCT.md).
