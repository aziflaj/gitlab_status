# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'gitlab_status/version'

Gem::Specification.new do |spec|
  spec.name = 'gitlab_status'
  spec.version = GitlabStatus::VERSION
  spec.authors = ['Aldo Ziflaj']
  spec.email = ['aldoziflaj95@gmail.com']

  spec.summary = 'A Ruby CLI to check the status of Gitlab'
  spec.description = <<~DESCRIPTION
    A small Ruby gem that exposes a CLI to check the status of https://gitlab.com or
    https://about.gitlab.com and reports an average response time after probing the site
    every 10 seconds for a one minute
  DESCRIPTION
  spec.homepage = ''
  spec.license = 'MIT'

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  raise 'RubyGems 2.0 or newer is required to protect against public gem pushes.' unless spec.respond_to?(:metadata)
  spec.metadata['allowed_push_host'] = 'http://mygemserver.com' # TODO: Set to some meaningful value

  spec.files = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir = 'bin'
  spec.executables = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = %w[lib]

  spec.add_runtime_dependency 'thor', '~> 0.20.0'

  spec.add_development_dependency 'bundler', '~> 1.15'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'simplecov', '~> 0.15.1'
end
