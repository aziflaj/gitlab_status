require 'thor'
require 'net/http'

module GitlabStatus
  class CLI < Thor
    desc 'scan', 'Checks the status of Gitlab'
    def scan
      records = []
      6.times do
        records << time_or_error
        sleep 10
      end
      print_results(records)
    end

    private

    def time_or_error(uri = 'https://gitlab.com/', limit = 10)
      start_time = Time.now
      response = Net::HTTP.get(URI(uri))
      case response
      when Net::HTTPUnknownResponse, Net::HTTPRequestTimeOut, Net::HTTPServerError, Net::HTTPBadResponse
        'Down'
      when Net::HTTPRedirection
        limit > 1 ? time_or_error(response['location'], limit - 1) : 'Too many redirects'
      else
        Time.now - start_time
      end
    rescue SocketError
      nil
    end

    def print_results(results)
      printf("%2s %-20s %s\n", '#', 'Elapsed time (s)', 'Status')
      sum = 0
      count = 0
      results.each_with_index do |res, index|
        status = if res.is_a? Numeric
                   sum += res
                   count += 1
                   'OK'
                 else
                   'Bad response'
                 end
        printf("%2s %-20s %s\n", index, res, status)
      end
      avg = count.positive? ? "#{(sum / count).round(2)}s" : 'NaN'
      puts "Average response time: #{avg}"
    end
  end
end
