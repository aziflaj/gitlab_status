require 'spec_helper'

RSpec.describe GitlabStatus::CLI do
  before(:all) do
    @output = `ruby -Ilib bin/gitlab-status scan`
  end

  it 'prints 6 statuses per minute' do
    lines = @output.split("\n").count
    expect(lines).to eq(8)
  end

  it 'calculates average correctly' do
    times = @output.split("\n").map do |line|
      line.split(' ').reject(&:empty?)[1].to_f
    end
    times.reject!(&:zero?)
    calculated_avg = @output.split("\n").last.split(' ').last.to_f
    expect((times.sum / times.count).round(2)).to eq(calculated_avg)
  end
end
